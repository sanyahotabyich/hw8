def typed(foo):
    def wrapper(type='str'):
         if type is str:
            print(type(str) '= str')
            foo()
    return wrapper


@typed(type='str')
def add(a, b):
    return a + b


add(3, 5)
# add("3", 5) -> "8"
# add(5, 5) -> "10"
# add('a', 'b') -> 'ab’


'''
def my_decorator(func):
    def wrapper():
        print("Before")
        func()
        print("After")
    return wrapper


@my_decorator
def simple_function():
    print("Simple function")


simple_function()
'''